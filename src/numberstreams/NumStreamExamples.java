package numberstreams;

import java.util.IntSummaryStatistics;
import java.util.stream.IntStream;

/**
 * Created by szagoret on 25.05.2016.
 */
public class NumStreamExamples {
    public static void main(String[] args) {
        IntStream intStream = IntStream.of(1, 2, 3, 4, 5, 6, 7, 8, 9);

//        int sum = intStream.sum();
//
//        OptionalInt min = intStream.min();
//        OptionalInt max = intStream.max();
//        OptionalDouble average = intStream.average();
//
        IntSummaryStatistics intSummaryStatistics = intStream.summaryStatistics();

        System.out.println("intSummaryStatistics = " + intSummaryStatistics);
//        System.out.println("sum = " + sum);
//        System.out.println("min = " + min);
//        System.out.println("max = " + max);
//        System.out.println("average = " + average);
    }
}
