package parallel;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by szagoret on 26.05.2016.
 */
public class ParallelStream {
    public static void main(String[] args) {

        List<String> strings = new CopyOnWriteArrayList<>();
        List<String> collect = Stream.iterate("+", s -> s + "+").parallel()
                .limit(1000)
//                .peek(s -> System.out.println(s + " processed in the thread " + Thread.currentThread().getName()))
//                .forEach(s -> strings.add(s));
                .collect(Collectors.toList());

//        System.out.println("strings = " + strings.size());
        System.out.println("# collect = " + collect.size());
    }
}
