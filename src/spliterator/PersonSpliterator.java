package spliterator;

import model.Person;

import java.util.Spliterator;
import java.util.function.Consumer;

/**
 * Created by szagoret on 5/14/2016.
 */
public class PersonSpliterator implements Spliterator<Person> {

    private final Spliterator<String> lineSpliterator;
    private String name;
    private int age;
    private String city;

    public PersonSpliterator(Spliterator<String> lineSpliterator) {
        this.lineSpliterator = lineSpliterator;
    }

    @Override
    public boolean tryAdvance(Consumer<? super Person> action) {

        /**
         * tryAdvance return boolean if it has elements to consume
         * if all 3 tryAdvance methods return true then we have read a person from file
         * and we can call accept function and return true
         * else we return false
         */
        if (this.lineSpliterator.tryAdvance(line -> this.name = line) &&
                this.lineSpliterator.tryAdvance(line -> this.age = Integer.parseInt(line)) &&
                this.lineSpliterator.tryAdvance(line -> this.city = line)) {

            Person person = new Person(name, age, city);
            action.accept(person);
            return true;
        }
        return false;
    }

    @Override
    public Spliterator<Person> trySplit() {
        // only for parallel processing
        return null;
    }

    @Override
    public long estimateSize() {
        // each person have 3 lines in file,
        // so we need to divide to 3 to compute the number of people
        return lineSpliterator.estimateSize() / 3;
    }

    @Override
    public int characteristics() {
        return lineSpliterator.characteristics();
    }
}
